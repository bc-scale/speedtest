#FROM ubuntu:focal
FROM ubuntu:focal

#https://hub.docker.com/_/ubuntu/

# File Author / Maintainer
MAINTAINER huezohuezo1990 <huezohuezo.1990@gmail.com>

# datos del Usuario
ARG USUARIO=huezohuezo1990
ARG UID=1990
ARG GID=1990
# Clave para el Usuario
ARG CLAVE=huezohuezo1990
#Creacion Usuario
RUN useradd -m ${USUARIO} --uid=${UID} && echo "${USUARIO}:${CLAVE}" | chpasswd
#Cambiar de /bin/sh a /bin/bash 
RUN usermod -s /bin/bash ${USUARIO}
# crear $HOME
#USER ${UID}:${GID}
#WORKDIR /home/${USUARIO}

WORKDIR /speedtest
RUN chmod -R 777 /speedtest 
RUN chown -R ${UID}:${GID} /speedtest

#ENV DEBIAN_FRONTEND noninteractive
ENV DEBIAN_FRONTEND noninteractive
ENV TZ=America/El_Salvador




# Set the timezone.
ENV TZ=America/El_Salvador
RUN apt-get update 
RUN apt-get install -y tzdata 
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime 
RUN echo $TZ > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata


# Update the repository sources list
RUN apt-get update

#Usuario de Ubuntu como sudo
RUN apt install sudo -y
RUN usermod -aG sudo ${USUARIO}

# SUDO sin Clave
RUN sed -i '/%sudo[[:space:]]/ s/ALL[[:space:]]*$/NOPASSWD:ALL/' /etc/sudoers

# speedtest
COPY speedtest speedtest
RUN chmod -R 777 /speedtest 
RUN chown -R ${UID}:${GID} /speedtest

# Install nano
RUN apt-get install -y nano && apt-get clean

# Install  net-tools + curl + wget 
RUN apt-get install -y net-tools curl wget && apt-get clean

# ping

RUN apt install -y iputils-ping && apt-get clean

# nmap

RUN apt install -y nmap && apt-get clean

# nmap

RUN apt-get install vim -y



#Iniciar speedtest
ENTRYPOINT ["/speedtest/speedtest"]
CMD []


